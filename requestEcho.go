package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
)

// PropertyItem Key and Value pair
type PropertyItem struct {
	Name  string
	Value string
}

// EchoResponse return content
type EchoResponse struct {
	Headers     []PropertyItem
	Cookies     []PropertyItem
	QueryParams []PropertyItem
}

// Echo returns the http request in a format
func Echo(w http.ResponseWriter, r *http.Request) {
	ret := new(EchoResponse)
	for name, headers := range r.Header {
		for _, h := range headers {
			ret.Headers = append(ret.Headers, PropertyItem{Name: name, Value: h})
		}
	}

	for name, quries := range r.URL.Query() {
		for _, v := range quries {
			ret.QueryParams = append(ret.QueryParams, PropertyItem{Name: name, Value: v})
		}
	}

	for _, cookie := range r.Cookies() {
		ret.Cookies = append(ret.Cookies, PropertyItem{Name: cookie.Name, Value: cookie.Value})
	}

	defer r.Body.Close()

	j, err := json.Marshal(ret)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(j))
	}
}

// VerifyHeader Handles unauthorized calls
func VerifyHeader(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Header.Get("API_Key") == os.Getenv("API_Key") {
			next.ServeHTTP(w, r)
		} else {
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Unauthorized"))
		}
	}
}

func main() {
	http.HandleFunc("/", VerifyHeader(Echo))
	log.Fatal(http.ListenAndServe(":80", nil))
}
